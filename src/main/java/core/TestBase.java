package core;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import util.Get_XL_Data;
import util.Get_XML_Data;

// Super class for all Test Classes
public class TestBase {
	
	protected WebDriver driver;
	private Properties config;
	private Get_XL_Data xlData;
	protected Reporter report;
	
	@BeforeSuite
	public void setTestEnv() {
		try {
			config = new Properties();
			config.load(new FileInputStream("./TestConfig.properties"));
			report = new Reporter();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@BeforeMethod
	public void startReporting(Method method) {
		report.startTest(method.getName());
	}
	
	@AfterMethod
	public void endReporting() {
		report.endTest();
	}
	
	@AfterSuite	
	public void flushReport() {
		report.flush();
	}
		
	@BeforeMethod
	@DataProvider(name="testdata")
	public Object[][] prepData(Method method) {
		Map<Integer,HashMap<String,String>> dataSet;
		Object[][] Arrayobj = null;
		String moduleName = method.getDeclaringClass().getSimpleName();
		String testName = method.getName();
		IGetData data = null;
		if(getTestProp("DATA_SOURCE").equalsIgnoreCase("EXCEL")) {
				data = new Get_XL_Data();
		}else if(getTestProp("DATA_SOURCE").equalsIgnoreCase("XML")) {
				data = new Get_XML_Data();
		}
		dataSet = data.Read_Data(moduleName, testName);
		if (dataSet != null) {
			Arrayobj = new Object[dataSet.size()][2];
			int count = 0;
			for(Map.Entry<Integer,HashMap<String,String>> td : dataSet.entrySet()) {
				Arrayobj[count][0] = td.getKey();
				Arrayobj[count][1] = td.getValue();
				count++;
			}
		} 
		return Arrayobj;
	}
	
	@BeforeClass
	public void initDriver() {
		try {
			String browser = getTestProp("BROWSER");	
			switch(browser.toUpperCase()) {
			case "FIREFOX" : 
				System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"//drivers//geckodriver.exe");
				driver = new FirefoxDriver(); break;
			case "CHROME"  :
				System.setProperty("webdriver.chrome.driver", 
						System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
				driver = new ChromeDriver(); break;
			case "IE"  :
				System.setProperty("webdriver.ie.driver", 
						System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
				driver = new InternetExplorerDriver(); break;		
			default : throw new Exception("Invalid browser :"+browser);
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(Long.parseLong(getTestProp("PAGE_TIMEOUT"))
									 , TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(Long.parseLong(getTestProp("IMPLICIT_WAIT"))
									 , TimeUnit.SECONDS);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@AfterClass
	public void quitDriver() {
		if(driver!=null) driver.quit();
	}
	
	protected String getTestProp(String propName) {
		if(config!=null & !config.isEmpty()) {
			if(config.containsKey(propName.toUpperCase())) {
			   return config.getProperty(propName.toUpperCase());
			}
		}
		return null;
	}
	
	
	

}
