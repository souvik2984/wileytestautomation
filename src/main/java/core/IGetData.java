package core;

import java.util.HashMap;
import java.util.Map;

public interface IGetData {
	public Map<Integer,HashMap<String,String>> Read_Data(String Module, String TestCase);
}
