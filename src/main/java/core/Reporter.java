package core;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reporter {
	
	ExtentReports extent = null;
	ExtentTest test = null;
	
	public Reporter() {
		extent = new ExtentReports(System.getProperty("user.dir")+
						  "//TestReports//TestReport.html", true);
	}
	
	public void startTest (String testName) {
		test = extent.startTest(testName);
	}
	
	public void log(ReportStatus status,String stepDetails) {
		switch(status) {
			case PASS : test.log(LogStatus.PASS, stepDetails);break;
			case FAIL : test.log(LogStatus.FAIL, stepDetails);break;
			case WARN : test.log(LogStatus.WARNING, stepDetails);break;
		}
	}
	
	public void endTest() {
		extent.endTest(test);
	}
	
	public void flush() {
		extent.flush();
	}

}
