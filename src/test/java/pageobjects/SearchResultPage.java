package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage extends PageBase{
	
	@FindBy(xpath=".//div[@class='search__result search__result--space']")
	private WebElement searchedResult;
	
	@FindBy(xpath="(.//img[@alt='Wiley Online Library'])[1]")
	private WebElement homeImg;

	public SearchResultPage(WebDriver driver) {
		super(driver);
	}
	
	public boolean verifyResult(String expectedText) {
		return waitForElement(driver,searchedResult,2).getText().contains(expectedText);
	}
	
	public void navigateToHome() {
		homeImg.click();
	}
	
	
}
