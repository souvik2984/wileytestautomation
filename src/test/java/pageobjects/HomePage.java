package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends PageBase{

	@FindBy(xpath=".//input[@name='AllField']")
	private WebElement TXT_SEARCH;
	
	@FindBy(xpath=".//button[@title='Search']")
	private WebElement BTN_SEARCH;
	
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	public void launchApp(String URL) {
		driver.get(URL);
	}
	
	public boolean isHomePage() {
		return waitForElement(driver,TXT_SEARCH,5).isDisplayed();
	}
	
	public SearchResultPage performSearch(String searchText) {
		TXT_SEARCH.sendKeys(searchText);
		BTN_SEARCH.click();
		return new SearchResultPage(driver);
	}
	
	
	
	
}
