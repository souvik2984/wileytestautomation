package testcases;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import core.ReportStatus;
import core.TestBase;
import pageobjects.HomePage;
import pageobjects.SearchResultPage;

// Home Test Cases
public class Home extends TestBase{
	private HomePage home;
	private SearchResultPage result;
	
	@BeforeClass
	public void initTest() {
		home = new HomePage(driver);
		home.launchApp(getTestProp("URL_"+getTestProp("ENV")));
	}

	@Test(dataProvider = "testdata")
	public void Verify_Search_Title(int itr,HashMap<String,String> td) {
			if(home.isHomePage()) {
				report.log(ReportStatus.PASS,"Home Page launched");
			}else {
				report.log(ReportStatus.FAIL,"Unable to access Home page");
			}
			result = home.performSearch(td.get("SearchTitle"));
			if(result.verifyResult(td.get("SearchTitle"))){
				report.log(ReportStatus.PASS,"Searched result verified successfully for "+td.get("SearchTitle"));
			}else {
				report.log(ReportStatus.FAIL,"Searched result didnt find the word"+td.get("SearchTitle") );
			}
			result.navigateToHome();
	}
	
	
	
	
	
	
	

}
